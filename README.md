# FlexPorter

The FlexPorter is an excel data importing library capable of auto-detecting header rows and object types. It grants the ability to read data from excel files in a simple and lightweight manner.

The library is available on nuget.org (https://api.nuget.org/v3/index.json) under the name FlexPorter.

FlexPorter is capable of scanning excel files in order to find a row of column headers with data rows underneath it:

![alt text](https://i.imgur.com/eSYsZ5k.png)

![alt text](https://i.imgur.com/QMJfQak.png)

The column headers correspond to properties marked with the ExcelImportField attribute in a C# class that implements the IExcelImportRecord interface:

![alt text](https://i.imgur.com/hr25ffz.png)

In addition to the auto-detection of the header row location, XL porter supports the auto-detection from multiple candidate object types.

# Usage

After defining a class with marked ExcelImportField attributes and instantiating an ExcelImporter, importing an excel file using auto-detection of column header row location is a simple matter of calling the HandleImport function:

![alt text](https://i.imgur.com/xYFBpfT.png)

![alt text](https://i.imgur.com/r4GkwJd.png)

This function can be called with or without a generic type argument. If no type argument is provided, the type is auto-detected.

Any classes that extend from IExcelImportRecord that are defined in the project that the ExcelImporter is instantiated in are candidates for auto-detection.

Whichever class has the most ExcelImportFields matching the column headers in the sheet is used for the import.

The candidate classes are found using Reflection in the constructor of ExcelImporter, so there is no need to supply a list to the ExcelImporter separately:

![alt text](https://i.imgur.com/Tp97taR.png)

![alt text](https://i.imgur.com/hDrpXpL.png)

# Specialized import functions

It is possible to supply specialized import functions for types that have non-standard import mechanisms. This is done as follows:

![alt text](https://i.imgur.com/TyIC08X.png)

The import mechanism can then be user-defined by writing a method with the following signature:

![alt text](https://i.imgur.com/DbOfHrM.png)

Use the GetPropertyByImportField method to find the PropertyInfo that corresponds to anExcelImportField attribute on your import class:

![alt text](https://i.imgur.com/FDI8KQu.png)

This method returns null if no matching property was found.

The headerRowInfo argument will be filled in when type auto-detection is used. Otherwise, the argument is null and you can fill it using your own header detection algorithm.

To use the standard auto-detection algorithm, use the GetBestHeaderRowInfo function.

The HeaderRowInfo class contains the type of the object, index of the header row, the string values of the entire row (including blanks or non header values) and a list of the found headers (excluding other values):

![alt text](https://i.imgur.com/qlGEG0v.png)

# Data tranformer

It is possible to specify a method that is called on all read values, that transforms the value before it is written to the object's properties. This is done as follows:

![alt text](https://i.imgur.com/GJ1igTa.png)

![alt text](https://i.imgur.com/epxXHed.png)

ExcelImport.DefaultTransformer can be used to apply the default tranformation function, which is currently defined as follows:

![alt text](https://i.imgur.com/7TNRdJB.png)

