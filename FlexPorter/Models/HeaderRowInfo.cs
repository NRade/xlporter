﻿using System;
using System.Collections.Generic;

namespace FlexPorter.Models
{
	public class HeaderRowInfo
	{
		public Type ImportType { get; set; }

		public int HeaderRowIndex { get; set; }

		public string[] HeaderRow { get; set; } = new string[0];

		public List<string> FoundHeaders { get; set; } = new List<string>();
	}
}
