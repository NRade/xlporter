﻿using ExcelDataReader;
using FastMember;
using FlexPorter.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;

namespace FlexPorter
{
	public class ExcelImporter
	{
		public List<Type> ImportClasses { get; set; }

		public Dictionary<Type, Func<DataTable, HeaderRowInfo, List<IExcelImportRecord>>> AlternativeImports { get; set; }
			= new Dictionary<Type, Func<DataTable, HeaderRowInfo, List<IExcelImportRecord>>>();

		public ExcelImporter()
		{
			ImportClasses = AppDomain.CurrentDomain.GetAssemblies().SelectMany(asm =>
			{
				try
				{
					return asm.GetTypes().Where(t => t.IsClass && typeof(IExcelImportRecord).IsAssignableFrom(t));
				}
				catch
				{
					return new Type[0];
				}
			}).ToList();
		}

		public List<T> HandleImport<T>(Stream receivedStream, ExcelImportOptions options) where T : IExcelImportRecord, new()
		{
			options.ObjectType = typeof(T);
			return HandleImport(receivedStream, options).Cast<T>().ToList();
		}

		public List<IExcelImportRecord> HandleImport(Stream receivedStream, ExcelImportOptions options)
		{
			List<IExcelImportRecord> output = new List<IExcelImportRecord>();

			using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(receivedStream))
			{
				DataSet dataSet = reader.AsDataSet();

				List<DataTable> sheets = dataSet.Tables.OfType<DataTable>().ToList();
				for (int i = 0; i < sheets.Count; i++)
				{
					bool match = (options.SheetIndexes ?? new int[0]).Contains(i) || (options.SheetNames ?? new string[0]).Contains(sheets[i].TableName);
					if (match == options.excludeSheetsMode)
					{
						sheets.RemoveAt(i);
						i--;
					}
				}

				foreach (DataTable sheet in sheets)
				{
					if (sheet.Rows.Count == 0)
					{
						return new List<IExcelImportRecord>();
					}

					if (options.ObjectType != null && AlternativeImports.ContainsKey(options.ObjectType))
					{
						return AlternativeImports[options.ObjectType](sheet, null);
					}

					HeaderRowInfo bestHeaderRowInfo = GetBestHeaderRowInfo(sheet, options);
					options.ObjectType = bestHeaderRowInfo.ImportType;

					if (bestHeaderRowInfo.FoundHeaders.Count < 2)
					{
						throw new NoSupportedTypeException();
					}

					if (AlternativeImports.ContainsKey(options.ObjectType))
					{
						return AlternativeImports[options.ObjectType](sheet, bestHeaderRowInfo);
					}

					output.AddRange(HandleStandardImport(sheet, options, bestHeaderRowInfo));
				}
			}

			return output;
		}

		public HeaderRowInfo GetBestHeaderRowInfo(DataTable sheet, ExcelImportOptions options)
		{
			return GetHeaderRowInfo(sheet, options).OrderBy(hri => hri.FoundHeaders.Count).Last();
		}

		public List<HeaderRowInfo> GetHeaderRowInfo(DataTable sheet, ExcelImportOptions options)
		{
			List<Type> candidateTypes = options.CandidateTypes;
			if (candidateTypes == null || candidateTypes.Count == 0)
			{
				candidateTypes = ImportClasses;
			}

			List<HeaderRowInfo> headerRowInfos = candidateTypes.Select(t => new HeaderRowInfo { ImportType = t, HeaderRowIndex = -1 }).ToList();

			int rowsToScan = options.RowsToScanForHeaders.HasValue ? Math.Min(options.RowsToScanForHeaders.Value, sheet.Rows.Count) : sheet.Rows.Count;

			for (int i = 0; i < rowsToScan; i++)
			{
				DataRow currentRow = sheet.Rows[i];

				foreach (HeaderRowInfo headerRowInfo in headerRowInfos)
				{
					if (headerRowInfo.FoundHeaders.Count >= options.EnoughHeaderMatches)
					{
						continue;
					}

					IExcelImportRecord staticRecord = (IExcelImportRecord)Activator.CreateInstance(headerRowInfo.ImportType);
					ITransformableExcelImportRecord transformable = staticRecord as ITransformableExcelImportRecord;

					string[] headerRow = currentRow.ItemArray.Select(item => ((transformable?.TransformPropertyValue(item, typeof(string)) ?? DefaultTransformer(item, typeof(string))) as string ?? "")
					.ToLower()).ToArray();
					List<string> foundHeaders = headerRow.Where(item => !string.IsNullOrEmpty(item) && GetPropertyByImportField(staticRecord, item) != null).ToList();

					if (foundHeaders.Count > headerRowInfo.FoundHeaders.Count)
					{
						headerRowInfo.HeaderRow = headerRow;
						headerRowInfo.FoundHeaders = foundHeaders;
						headerRowInfo.HeaderRowIndex = i;
					}
				}
			}

			return headerRowInfos;
		}

		public virtual object DefaultTransformer(object input, Type type)
		{
			try
			{
				if (type == typeof(string))
				{
					return Convert.ToString(input).Trim();
				}
				return Convert.ChangeType(input, Nullable.GetUnderlyingType(type) ?? type);
			}
			catch
			{
				return null;
			}
		}

		private readonly Dictionary<string, PropertyInfo> CachedPropertyInfo = new Dictionary<string, PropertyInfo>();

		public PropertyInfo GetPropertyByImportField(IExcelImportRecord importRecord, string importField)
		{
			PropertyInfo foundPI = null;

			string combinedKey = $"{importField}-{importRecord.GetType().FullName}";

			if (CachedPropertyInfo.TryGetValue(combinedKey, out foundPI))
			{
				return foundPI;
			}

			PropertyInfo[] pis = importRecord.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

			IMappedExcelImportRecord mappedRecord = importRecord as IMappedExcelImportRecord;
			if (mappedRecord != null)
			{
				foundPI = pis.FirstOrDefault(pi => mappedRecord.MapPropertyName(importField) == pi.Name);
			}

			if (foundPI == null)
			{
				foundPI = pis.FirstOrDefault(p => p.GetCustomAttributes<ExcelImportProperty>().Select(f => f.HeaderName.ToLower()).FirstOrDefault() == importField);
			}

			if (foundPI == null && importRecord.GetType().GetCustomAttributes<MapByPascalCaseName>(true).Any())
			{
				foundPI = pis.FirstOrDefault(p => p.Name == ToPascalCase(importField));
			}

			CachedPropertyInfo.Add(combinedKey, foundPI);

			return foundPI;
		}

		private static string ToPascalCase(string input)
		{
			string yourString = input.ToLower().Replace("_", " ");
			TextInfo info = CultureInfo.CurrentCulture.TextInfo;
			return info.ToTitleCase(yourString).Replace(" ", string.Empty);
		}

		private List<IExcelImportRecord> HandleStandardImport(DataTable sheet, ExcelImportOptions options)
		{
			return HandleStandardImport(sheet, options, GetBestHeaderRowInfo(sheet, options));
		}



		public List<IExcelImportRecord> HandleStandardImport(DataTable sheet, ExcelImportOptions options, HeaderRowInfo headerRowInfo)
		{
			List<IExcelImportRecord> importRecords = new List<IExcelImportRecord>();

			TypeAccessor accessor = TypeAccessor.Create(options.ObjectType);

			for (int i = headerRowInfo.HeaderRowIndex + 1; i < sheet.Rows.Count; i++)
			{
				DataRow currentRow = sheet.Rows[i];

				IExcelImportRecord importRecord = (IExcelImportRecord)Activator.CreateInstance(options.ObjectType);
				ISettableExcelImportRecord settableRecord = importRecord as ISettableExcelImportRecord;
				ITransformableExcelImportRecord transformableRecord = importRecord as ITransformableExcelImportRecord;
				IExtendedExcelImportRecord extendedRecord = importRecord as IExtendedExcelImportRecord;

				if (extendedRecord != null && extendedRecord.OtherValues == null)
				{
					extendedRecord.OtherValues = new Dictionary<string, object>();
				}

				bool propertiesSet = false;

				for (int j = 0; j < currentRow.ItemArray.Length; j++)
				{
					object value = currentRow.ItemArray[j];

					if (j < headerRowInfo.HeaderRow.Length && value != null && value != DBNull.Value)
					{
						string header = headerRowInfo.HeaderRow[j];

						PropertyInfo property = GetPropertyByImportField(importRecord, header);
						try
						{
							Type targetType = property?.PropertyType ?? value?.GetType();

							object transformedValue = transformableRecord?.TransformPropertyValue(value, targetType) ?? DefaultTransformer(value, targetType);

							if (transformedValue != null)
							{
								if (settableRecord != null)
								{
									settableRecord.SetPropertyValue(header, transformedValue);
									propertiesSet = true;
								}
								else if (property != null)
								{
									accessor[importRecord, property.Name] = transformedValue;
									propertiesSet = true;
								}
								else if (extendedRecord != null)
								{
									extendedRecord.OtherValues.Add(header, transformedValue);
									propertiesSet = true;
								}
							}
						}
						catch (Exception)
						{
						}
					}
				}

				if (!propertiesSet)
				{
					break;
				}

				importRecords.Add(importRecord);
			}

			return importRecords;
		}
	}
}
