﻿namespace FlexPorter.Models
{
    public interface ISettableExcelImportRecord : IExcelImportRecord
    {
        void SetPropertyValue(string headerName, object value);
    }
}
