﻿using System;

namespace FlexPorter.Models
{
    public class InvalidOptionsException : Exception
    {
        public InvalidOptionsException(string message) : base(message)
        {

        }
    }
}
